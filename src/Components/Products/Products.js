import React from "react";
import Card from "../Card/Card";
import "./Products.css";
import { InfinitySpin } from "react-loader-spinner";

function Products({
  handleCart,
  API_STATES,
  productsDetail,
}) {
  return productsDetail.status === API_STATES.LOADING ? (
    <div className="spinner">
      <InfinitySpin width="200" color="#4fa94d" />
    </div>
  ) : productsDetail.status === API_STATES.ERROR ? (
    <h1 className="item-align">Data not Fetched</h1>
  ) : productsDetail.length === 0 ? (
    <h1 className="item-align">Internal server error</h1>
  ) : (
    <div className="products">
      {productsDetail.productsData.map((product) => {
        return (
          <Card
            product={product}
            key={product.id}
            handleCart={handleCart}
          />
        );
      })}
    </div>
  );
}

export default Products;
