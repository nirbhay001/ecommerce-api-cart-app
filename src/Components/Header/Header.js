import React from "react";
import "./Header.css";
import { Link } from "react-router-dom";

function Header({ cartState }) {
  const cartItemLength = cartState.length;
  return (
    <div className="header">
      <span>
        <i className="fa-solid fa-house cart-icon"></i>
      </span>
      <span className="product-type">
        <Link to="/">
          <span className="product-link">Home</span>
        </Link>
        <span className="product-link">Electronics</span>
        <span className="product-link">Grocerry</span>
      </span>
      <span className="cart-icon">
        <Link to="/cart">
          <i className="fa-solid fa-cart-shopping .bg-dark">
            <span className="cartItem">{cartItemLength}</span>
          </i>
        </Link>
      </span>
    </div>
  );
}

export default Header;
