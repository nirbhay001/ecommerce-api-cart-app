import React from "react";
import "./Card.css";
import { Link } from "react-router-dom";

function Card({ product, handleCart }) {
  return (
    <div className="card" key={product.id}>
      <Link to={`/product/${product.id}`}>
        <img className="card-image" src={product.image} alt="product-image" />
      </Link>
      <div>
        Price<span className="text-bold"> : {product.price}$</span>
      </div>
      <div>{product.title}</div>
      <div>
        Rating<span className="text-bold"> : {product.rating.rate}</span>
      </div>
      <div>
        Category<span className="text-bold"> : {product.category}</span>
      </div>
      <div>
        Count<span className="text-bold"> : {product.rating.count}</span>
      </div>
      <button className="btn btn-success" onClick={() => handleCart(product)}>
        Add to Cart
      </button>
    </div>
  );
}

export default Card;
