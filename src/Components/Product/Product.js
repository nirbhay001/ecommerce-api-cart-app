import React from "react";
import "./Product.css";
import { Link, useParams } from "react-router-dom";
import { InfinitySpin } from "react-loader-spinner";

function Product({ productsDetail, handleCart, API_STATES }) {
  let { productId } = useParams();
  let product = productsDetail.productsData.filter((cardItem) => {
    return cardItem.id === +productId;
  })[0];

  return productsDetail.status === API_STATES.LOADING ? (
    <div className="spinner">
      <InfinitySpin width="200" color="#4fa94d" />
    </div>
  ) : productsDetail.status === API_STATES.ERROR ? (
    <h1 className="item-align">Data not Fetched</h1>
  ) : productsDetail.length === 0 ? (
    <h1 className="item-align">Internal server error</h1>
  ) : (
    <div className="product">
      <Link to="/">
        <button className="btn btn-success">Back to Home</button>
      </Link>
      <div className="product-header" key={product.key}>
        <img src={product.image} alt="product-image" className="product-img" />
        <div>
          Price<span className="text-bold"> : {product.price}$</span>
        </div>
        <div>{product.title}</div>
        <div>
          Rating<span className="text-bold"> : {product.rating.rate}</span>
        </div>
        <div>{product.description}</div>
        <div>
          Category<span className="text-bold"> : {product.category}</span>
        </div>
        <div>
          Count<span className="text-bold"> : {product.rating.count}</span>
        </div>
        <button className="btn btn-success" onClick={() => handleCart(product)}>
          Add to Cart
        </button>
        <div></div>
      </div>
    </div>
  );
}

export default Product;
