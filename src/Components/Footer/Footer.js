import React from "react";
import "./Footer.css";

function Footer() {
  return (
    <div className="footer-container">
      <div className="footer">
        <div>
          All copyright reserved <span>&#169;</span>
        </div>
      </div>
    </div>
  );
}

export default Footer;
