import React from "react";
import { Link } from "react-router-dom";
import "./Cart.css";

function Cart({ cartState, setCartState }) {
  let total = 0;
  const removeItem = (id) => {
    setCartState([
      ...cartState.filter((item) => {
        return item.id !== id;
      }),
    ]);
  };

  const itemPlus = (product) => {
    if (product.quantity >= 5) {
      return;
    }

    setCartState([
      ...cartState.map((cartItem) => {
        if (cartItem.id === product.id) {
          return {
            ...cartItem,
            quantity: cartItem.quantity + 1,
            price:
              Number(cartItem.price) +
              Number(cartItem.price) / cartItem.quantity,
          };
        } else {
          return cartItem;
        }
      }),
    ]);
  };

  const itemMinus = (product) => {
    if (product.quantity <= 1) {
      return;
    }
    setCartState([
      ...cartState.map((cartItem) => {
        if (cartItem.id === product.id) {
          return {
            ...cartItem,
            quantity: cartItem.quantity - 1,
            price:
              Number(cartItem.price) -
              Number(cartItem.price) / cartItem.quantity,
          };
        } else {
          return cartItem;
        }
      }),
    ]);
  };

  return (
    <div>
      <Link to="/">
        <button className="btn btn-success m-3">Back to Home</button>
      </Link>
      {cartState.length !== 0
        ? cartState.map((cartItem) => {
            total = total + cartItem.price;
            return (
              <div className="cart" key={cartItem.id}>
                <img
                  src={cartItem.image}
                  className="cart-image"
                  alt="image of product"
                ></img>
                <span className="description">{cartItem.description}</span>
                <span className="cartItem-price">
                  {cartItem.price.toFixed(2)}$
                </span>
                <span className="button-item">
                  <button
                    className="button-style"
                    onClick={() => itemPlus(cartItem)}
                  >
                    +
                  </button>
                  <div className="cartItem-quantity">{cartItem.quantity}</div>
                  <button
                    className="button-style"
                    onClick={() => itemMinus(cartItem)}
                  >
                    -
                  </button>
                </span>
                <button
                  className="bg-danger text-white"
                  onClick={() => removeItem(cartItem.id)}
                >
                  Remove Item
                </button>
              </div>
            );
          })
        : ""}
      <div className="total-price">
        <div>Total Price</div>
        <div className="totalPrice">{total.toFixed(2)}</div>
      </div>
    </div>
  );
}

export default Cart;
