import "./App.css";
import Products from "./Components/Products/Products";
import { Routes, Route } from "react-router-dom";
import Header from "./Components/Header/Header";
import Cart from "./Components/Cart/Cart";
import { useState, useEffect } from "react";
import Product from "./Components/Product/Product";
import Footer from "./Components/Footer/Footer";

function App() {
  const [cartState, setCartState] = useState([]);
  const API_STATES = {
    LOADING: "loading",
    LOADED: "loaded",
    ERROR: "error",
  };

  const [productsDetail, setProductsDetail] = useState({
    productsData: [],
    status: API_STATES.LOADING,
  });

  useEffect(() => {
    fetch("https://fakestoreapi.com/products")
      .then((res) => {
        return res.json();
      })
      .then((responseData) => {
        setProductsDetail((prev) => {
          return {
            ...prev,
            productsData: responseData,
            status: API_STATES.LOADED,
          };
        });
      })
      .catch((error) => {
        console.error(error.message);
        setProductsDetail((prev) => {
          return {
            ...prev,
            status: API_STATES.ERROR,
          };
        });
      });
  }, []);

  function handleCart(product) {
    const itemIndex = cartState.findIndex(
      (cartItem) => cartItem.id === product.id
    );
    if (itemIndex !== -1) {
      const updatedCartState = cartState.map((item, index) => {
        if (index === itemIndex) {
          return {
            ...item,
            quantity: item.quantity + 1,
            price: item.price + product.price,
          };
        } else {
          return item;
        }
      });
      setCartState(updatedCartState);
    } else {
      const updatedCartState = [...cartState, { ...product, quantity: 1 }];
      setCartState(updatedCartState);
    }
  }

  return (
    <>
      <Header cartState={cartState} />
      <Routes>
        <Route
          path="/"
          element={
            <Products
              handleCart={handleCart}
              API_STATES={API_STATES}
              productsDetail={productsDetail}
            />
          }
        ></Route>
        <Route
          path="/cart"
          element={<Cart cartState={cartState} setCartState={setCartState} />}
        ></Route>
        <Route
          path="/product/:productId"
          element={
            <Product
              productsDetail={productsDetail}
              handleCart={handleCart}
              API_STATES={API_STATES}
            />
          }
        ></Route>
      </Routes>
      <Footer />
    </>
  );
}
export default App;
